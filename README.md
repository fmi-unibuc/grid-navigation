# Running
```sh
cd release
java -jar gridNavigation.jar
```

# Editing the inputs
The app takes to different grids in two different grid files for comparison. The files are named `inputGrid1.txt` and `inputGrid2.txt` and must be in the same location as the JAR.
The starting position for the path algorithm is configured in `inputPositions.txt`. It, too, must be in the same location as the JAR.