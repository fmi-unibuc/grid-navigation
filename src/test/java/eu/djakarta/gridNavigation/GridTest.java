package eu.djakarta.gridNavigation;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;
import java.util.Objects;

import org.junit.jupiter.api.Test;

class GridTest {

  @Test
  void testHashCode() {
    Grid smallEmptyGrid = new Grid("2 2\n"
        + "0 0\n"
        + "0 0");
    assertEquals(smallEmptyGrid.hashCode(), smallEmptyGrid.hashCode());
    
    Grid smallNotEmptyGrid1 = new Grid("2 2\n"
        + "0 1\n"
        + "0 0");
    assertNotEquals(smallNotEmptyGrid1, new Grid(2, 2));
    
    Grid smallNotEmptyGrid2 = new Grid("2 2\n"
        + "0 1\n"
        + "2 0");
    assertEquals(smallEmptyGrid.hashCode(), new Grid(2, 2).hashCode());
    
    final int prime = 31;
    int result = 1;
    result = prime * result + Arrays.deepHashCode(new int[][] {{0, 1}, {0, 0}});
    result = prime * result + Objects.hash(2, 2, Grid.class);
    assertEquals(smallNotEmptyGrid1.hashCode(), result);
    
    assertNotEquals(new CellPosition(5, 6).hashCode(), Objects.hash(6, 5, Object.class));
  }

  @Test
  void testGridString() {
    fail("Not yet implemented");
  }

  @Test
  void testGridIntInt() {
    fail("Not yet implemented");
  }

  @Test
  void testGetCellCost() {
    fail("Not yet implemented");
  }

  @Test
  void testGetPathCost() {
    fail("Not yet implemented");
  }

  @Test
  void testGetLeftNeighbor() {
    Grid testGrid = new Grid(null);
    assertEquals(Grid.getLeftNeighbor(new CellPosition(0, 0), testGrid), new CellPosition(0, 0));
  }

  @Test
  void testGetTopNeighbor() {
    fail("Not yet implemented");
  }

  @Test
  void testGetRightNeighbor() {
    fail("Not yet implemented");
  }

  @Test
  void testGetBottomNeighbor() {
    fail("Not yet implemented");
  }

  @Test
  void testToString() {
    fail("Not yet implemented");
  }

  @Test
  void testEqualsObject() {
    Grid emptyGrid = new Grid(5, 10);
    assertEquals(emptyGrid, emptyGrid);
    assertEquals(emptyGrid, new Grid(5, 10));
    assertNotEquals(emptyGrid, new Grid(5, 9));
    assertNotEquals(emptyGrid, null);
    assertNotEquals(emptyGrid, new Object());
    assertNotEquals(new Grid(4, 9), new Grid(5, 9));

    Grid smallEmptyGrid = new Grid("2 2\n" + "0 0\n" + "0 0");
    assertEquals(smallEmptyGrid, new Grid(2, 2));

    Grid smallNotEmptyGrid1 = new Grid("2 2\n" + "0 1\n" + "0 0");
    assertNotEquals(smallNotEmptyGrid1, new Grid(2, 2));

    Grid smallNotEmptyGrid2 = new Grid("2 2\n" + "0 1\n" + "2 0");
    assertNotEquals(smallNotEmptyGrid2, new Grid(2, 2));
    assertNotEquals(smallNotEmptyGrid1, smallNotEmptyGrid2);
  }

}
