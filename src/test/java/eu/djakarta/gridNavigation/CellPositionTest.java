package eu.djakarta.gridNavigation;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Objects;

import org.junit.jupiter.api.Test;

class CellPositionTest {

  @Test
  void testHashCode() {
    assertEquals(new CellPosition(5, 6).hashCode(), new CellPosition(5, 6).hashCode());
    assertEquals(new CellPosition(5, 6).hashCode(), Objects.hash(6, 5, CellPosition.class));
    assertNotEquals(new CellPosition(5, 6).hashCode(), Objects.hash(6, 5, Object.class));
  }

  @Test
  void testCellPosition() {
    CellPosition cellPosition = new CellPosition(5, 6);
    assertEquals(cellPosition.line, 5);
    assertEquals(cellPosition.column, 6);
  }

  @Test
  void testToString() {
    assertEquals(new CellPosition(5, 6).toString(), "(" + 5 + ", " + 6 + ")");
  }
  
  @Test
  void testEqualsObject() {
    CellPosition cellPosition = new CellPosition(0, 0);
    assertEquals(cellPosition, cellPosition);
    assertEquals(cellPosition, new CellPosition(0, 0));
    assertNotEquals(cellPosition, new Object());
    assertNotEquals(cellPosition, null);
    assertNotEquals(cellPosition, new CellPosition(0, 1));
    assertNotEquals(new CellPosition(2, 1), new CellPosition(0, 1));
  }
  
}
