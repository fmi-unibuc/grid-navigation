package eu.djakarta.gridNavigation;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class GridPathTest {

  @Test
  void testGridPath() {
    assertEquals(new GridPath(new CellPosition(3, 4)).getStart(), new GridPath(3, 4).getStart());
    assertNotEquals(new GridPath(new CellPosition(3, 4)).getStart(), new GridPath(4, 4).getStart());
  }

  @Test
  void testGetStart() {
    assertEquals(new GridPath(3, 4).getStart(), new CellPosition(3, 4));
    assertNotEquals(new GridPath(3, 4).getStart(), new CellPosition(4, 4));
  }

  @Test
  void testGetEnd() {
    assertEquals(new GridPath(3, 4).getEnd(), new CellPosition(3, 4));
    assertNotEquals(new GridPath(3, 4).getEnd(), new CellPosition(4, 4));

    assertEquals(new GridPath(3, 4).add(Direction.DOWN).getEnd(), new CellPosition(3, 4));
    assertNotEquals(new GridPath(3, 4).add(Direction.DOWN).getEnd(), new CellPosition(4, 4));
  }

  @Test
  void testAddDirection() {
    fail("Not yet implemented");
  }

  @Test
  void testAddCellPosition() {
    fail("Not yet implemented");
  }

  @Test
  void testToPathString() {
    fail("Not yet implemented");
  }

  @Test
  void testGetCellUnvisitedNeighbors() {
    fail("Not yet implemented");
  }

  @Test
  void testGetDirectionFromCells() {
    fail("Not yet implemented");
  }

}
