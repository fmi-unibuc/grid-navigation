package eu.djakarta.gridNavigation;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class DirectionTest {

  @Test
  void testDirection() {
    assertEquals("LEFT", Direction.LEFT.name());
    assertEquals("UP", Direction.UP.name());
    assertEquals("RIGHT", Direction.RIGHT.name());
    assertEquals("DOWN", Direction.DOWN.name());
  }

  @Test
  void testToString() {
    assertEquals("left", Direction.LEFT.toString());
    assertEquals("up", Direction.UP.toString());
    assertEquals("right", Direction.RIGHT.toString());
    assertEquals("down", Direction.DOWN.toString());
  }

}
