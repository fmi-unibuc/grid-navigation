package eu.djakarta.gridNavigation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class GridPath {
  private final ArrayList<Direction> modifiableDirections = new ArrayList<Direction>();
  public final List<Direction> directions = Collections.unmodifiableList(modifiableDirections);

  private final ArrayList<CellPosition> modifiableCellPositions = new ArrayList<CellPosition>();
  public final List<CellPosition> cellPositions = Collections
      .unmodifiableList(modifiableCellPositions);

  public GridPath(CellPosition start) {
    this.modifiableCellPositions.add(start);
  }

  public GridPath(int startLine, int startColumn) {
    this(new CellPosition(startLine, startColumn));
  }

  public CellPosition getStart() {
    return cellPositions.get(0);
  }

  public CellPosition getEnd() {
    return cellPositions.get(cellPositions.size() - 1);
  }

  public GridPath add(Direction direction) {
    this.modifiableDirections.add(direction);
    this.modifiableCellPositions.add(null);
    return this;
  }

  public GridPath add(CellPosition cell) {
    return this.add(GridPath.getDirectionFromCells(this.getEnd(), cell));
  }

  public static String toPathString(ArrayList<CellPosition> pathArray) {
    StringBuilder stringBuilder = new StringBuilder();
    for (int i = 0; i < pathArray.size(); i++) {
      stringBuilder.append(pathArray.get(i) + "\n");
    }
    return stringBuilder.toString();
  }

  public static ArrayList<CellPosition> getCellUnvisitedNeighbors(CellPosition cellPosition,
      Grid grid, ArrayList<CellPosition> pathArray) {
    ArrayList<CellPosition> unvisitedNeighbors = new ArrayList<CellPosition>();

    if (cellPosition.line > 0) {
      CellPosition topNeighbor = new CellPosition(cellPosition.line - 1, cellPosition.column);
      if (!pathArray.contains(topNeighbor)) {
        unvisitedNeighbors.add(topNeighbor);
      }
    }
    if (cellPosition.line < grid.height - 1) {
      CellPosition bottomNeighbor = new CellPosition(cellPosition.line + 1, cellPosition.column);
      if (!pathArray.contains(bottomNeighbor)) {
        unvisitedNeighbors.add(bottomNeighbor);
      }
    }
    if (cellPosition.column > 0) {
      CellPosition leftNeighbor = new CellPosition(cellPosition.line, cellPosition.column - 1);
      if (!pathArray.contains(leftNeighbor)) {
        unvisitedNeighbors.add(leftNeighbor);
      }
    }
    if (cellPosition.column < grid.width - 1) {
      CellPosition rightNeighbor = new CellPosition(cellPosition.line, cellPosition.column + 1);
      if (!pathArray.contains(rightNeighbor)) {
        unvisitedNeighbors.add(rightNeighbor);
      }
    }

    return unvisitedNeighbors;
  }

  public static Direction getDirectionFromCells(CellPosition startCell, CellPosition endCell) {
    if (endCell.line == startCell.line && endCell.column == startCell.column - 1) {
      return Direction.LEFT;
    } else if (endCell.line == startCell.line - 1 && endCell.column == startCell.column) {
      return Direction.UP;
    } else if (endCell.line == startCell.line && endCell.column == startCell.column + 1) {
      return Direction.RIGHT;
    } else if (endCell.line == startCell.line + 1 && endCell.column == startCell.column) {
      return Direction.DOWN;
    } else {
      throw new RuntimeException(new IllegalArgumentException("The two passed argument cells ("
          + startCell + " and " + endCell + ") are not adjacent."));
    }
  }

  public static CellPosition getPositionFromDirection(CellPosition startCell, Direction direction) {
    return null;
  }

}
