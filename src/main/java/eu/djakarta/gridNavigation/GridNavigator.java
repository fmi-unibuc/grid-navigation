package eu.djakarta.gridNavigation;
import java.util.ArrayList;

public interface GridNavigator {
  public ArrayList<CellPosition> navigate(Grid grid, int startLine, int startColumn, int maximumLength);
}
