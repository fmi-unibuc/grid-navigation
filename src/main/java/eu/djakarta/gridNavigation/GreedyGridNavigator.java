package eu.djakarta.gridNavigation;

import java.util.ArrayList;

public class GreedyGridNavigator implements GridNavigator {

  @Override
  public ArrayList<CellPosition> navigate(Grid grid, int startLine, int startColumn,
      int maximumLength) {
    ArrayList<CellPosition> arrayPath = new ArrayList<CellPosition>();
    CellPosition currentCellPosition = new CellPosition(startLine, startColumn);
    arrayPath.add(currentCellPosition);
    ArrayList<CellPosition> currentCellNeighbors = GridPath
        .getCellUnvisitedNeighbors(currentCellPosition, grid, arrayPath);
    while (arrayPath.size() < maximumLength && currentCellNeighbors.size() > 0) {
      CellPosition mostRewardingNeighbor = GreedyGridNavigator
          .getBiggestValueCell(currentCellNeighbors, grid);
      arrayPath.add(mostRewardingNeighbor);
      currentCellPosition = mostRewardingNeighbor;
      currentCellNeighbors = GridPath.getCellUnvisitedNeighbors(currentCellPosition, grid,
          arrayPath);
    }
    return arrayPath;
  }

  private static CellPosition getSmallestValueCell(ArrayList<CellPosition> cells, Grid grid) {
    if (cells.size() == 0) {
      return null;
    }
    else {
      int leastCostPositionIndex = 0;
      for (int i = 1; i < cells.size(); i++) {
        if (grid.getCellCost(cells.get(i)) < grid.getCellCost(cells.get(leastCostPositionIndex))) {
          leastCostPositionIndex = i;
        }
      }
      return cells.get(leastCostPositionIndex);
    }
  }
  
  private static CellPosition getBiggestValueCell(ArrayList<CellPosition> cells, Grid grid) {
    if (cells.size() == 0) {
      return null;
    }
    else {
      int mostCostPositionIndex = 0;
      for (int i = 1; i < cells.size(); i++) {
        if (grid.getCellCost(cells.get(i)) > grid.getCellCost(cells.get(mostCostPositionIndex))) {
          mostCostPositionIndex = i;
        }
      }
      return cells.get(mostCostPositionIndex);
    }
  }

}
