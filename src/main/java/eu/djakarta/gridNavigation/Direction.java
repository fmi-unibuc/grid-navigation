package eu.djakarta.gridNavigation;

public enum Direction {
  LEFT("left"), UP("up"), RIGHT("right"), DOWN("down");

  private String label;

  Direction(String label) {
    this.label = label;
  }

  @Override
  public String toString() {
    return label;
  }
}
