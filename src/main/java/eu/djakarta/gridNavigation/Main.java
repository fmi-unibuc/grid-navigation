package eu.djakarta.gridNavigation;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
  public final static String grid1FileName = "inputGrid1.txt";
  public final static String grid2FileName = "inputGrid2.txt";
  public final static String positionsFileName = "inputPositions.txt";

  public static void main(String[] args) {
    int startLine;
    int startColumn;
    try {
      Scanner scanner = new Scanner(new File(Main.positionsFileName));
      startLine = scanner.nextInt();
      startColumn = scanner.nextInt();
      scanner.close();
    } catch (FileNotFoundException exception) {
      throw new RuntimeException(exception);
    }

    Grid grid1 = new Grid(getStringFromFile(Main.grid1FileName));
    Grid grid2 = new Grid(getStringFromFile(Main.grid2FileName));

    /*
     * System.out.println(Main.startLine); System.out.println(Main.startColumn);
     * System.out.println(grid);
     */

    printResults(grid1, startLine, startColumn);
    printResults(grid2, startLine, startColumn);
  }

  private static void printResults(Grid grid, int startLine, int startColumn) {
    ArrayList<CellPosition> greedyPath = new GreedyGridNavigator().navigate(grid, startLine,
        startColumn, 10);
    System.out.println("Path: " + greedyPath);
    System.out.println("Cost: " + grid.getPathCost(greedyPath));
    System.out.println("");
  }

  private static String getStringFromFile(String fileName) {
    try {
      Scanner scanner = new Scanner(new File(fileName));
      scanner.useDelimiter("\\Z");
      String gridFileContents = scanner.next();
      scanner.close();
      return gridFileContents;
    } catch (FileNotFoundException exception) {
      throw new RuntimeException(exception);
    }
  }
}
