package eu.djakarta.gridNavigation;

import java.util.Objects;

public class CellPosition {
  public final int line;
  public final int column;
  
  public CellPosition(int line, int column) {
    super();
    this.line = line;
    this.column = column;
  }

  @Override
  public String toString() {
    return "(" + this.line + ", " + this.column + ")";
  }
  
  @Override
  public int hashCode() {
    return Objects.hash(column, line, this.getClass());
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    CellPosition other = (CellPosition) obj;
    return column == other.column && line == other.line;
  }
}
