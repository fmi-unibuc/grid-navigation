package eu.djakarta.gridNavigation;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;
import java.util.Scanner;

public class Grid {
  public final int width;
  public final int height;
  public final int grid[][];
  

  public Grid(String gridString) {
    Scanner gridScanner = new Scanner(gridString);
    this.width = gridScanner.nextInt();
    this.height = gridScanner.nextInt();
    this.grid = new int[height][width];
    for (int i = 0; i < this.height; i++) {
      for (int j = 0; j < this.width; j++) {
        this.grid[i][j] = gridScanner.nextInt();
      }
    } 
    gridScanner.close();
  }
  
  public Grid(int width, int height) {
    this.width = width;
    this.height = height;
    this.grid = new int[height][width];
  }
  
  public int getCellCost(CellPosition cellPosition) {
    return this.grid[cellPosition.line][cellPosition.column];
  }
  
  public int getPathCost(ArrayList<CellPosition> pathArray) {
    int cost = 0;
    for (int i = 0; i < pathArray.size(); i++) {
      cost += this.getCellCost(pathArray.get(i));
    }
    return cost;
  }
  
  public static CellPosition getLeftNeighbor(CellPosition cellPosition, Grid grid) {
    return new CellPosition(cellPosition.line, cellPosition.column);
  }
  
  public static CellPosition getTopNeighbor(CellPosition cellPosition, Grid grid) {
    return new CellPosition(cellPosition.line, cellPosition.column);
  }
  
  public static CellPosition getRightNeighbor(CellPosition cellPosition, Grid grid) {
    return new CellPosition(cellPosition.line, cellPosition.column);
  }
  
  public static CellPosition getBottomNeighbor(CellPosition cellPosition, Grid grid) {
    return new CellPosition(cellPosition.line, cellPosition.column);
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    for (int i = 0; i < this.height; i++) {
      for (int j = 0; j < this.width; j++) {
        stringBuilder.append(this.grid[i][j] + " ");
      }
      stringBuilder.append("\n");
    }
    return stringBuilder.toString();
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + Arrays.deepHashCode(grid);
    result = prime * result + Objects.hash(height, width, this.getClass());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    Grid other = (Grid) obj;
    return Arrays.deepEquals(grid, other.grid) && height == other.height && width == other.width;
  }
}
